var contador = 0;
var continuar = 1;

// Realiza uma jogada
function selecionar(botao) 
{
    //var botao = document.getElementsByClassName("casa")
    if (botao.innerHTML == "" && continuar == 1) {
        if (contador%2 == 0) {
            botao.innerHTML = "O";
            document.getElementById("indicadorDaVez").innerHTML = "X";
            contador++;
            ganhou()
        }else
        {
            botao.innerHTML = "X";
            document.getElementById("indicadorDaVez").innerHTML = "O";
            contador++;
            ganhou()
        }
    }
}

// Zera todos as posições e recomeça o jogo
function resetar()
{
    contador = 0
    continuar = 1
    document.getElementById("indicadorDaVez").innerHTML = "O"
    document.getElementById("indicadorVencedor").innerHTML="O vencedor é..."
    var bot = document.getElementsByClassName("casa")
    for ( var i = 0; i < 9; i++)
    {
        bot[i].innerHTML = ""
    }
}

// Verifica todos as combinações possíveis de se ganhar o jogo
function ganhou() {
    var i = document.getElementsByClassName("casa")
    if ( i[0].innerHTML==i[1].innerHTML && i[2].innerHTML == "X" && i[2].innerHTML==i[1].innerHTML || i[3].innerHTML==i[4].innerHTML && i[5].innerHTML == "X" && i[4].innerHTML==i[5].innerHTML || i[6].innerHTML==i[7].innerHTML && i[8].innerHTML == "X" && i[7].innerHTML==i[8].innerHTML || i[0].innerHTML==i[3].innerHTML && i[6].innerHTML == "X" && i[6].innerHTML==i[3].innerHTML || i[1].innerHTML==i[4].innerHTML && i[7].innerHTML == "X" && i[7].innerHTML==i[1].innerHTML || i[2].innerHTML==i[5].innerHTML && i[8].innerHTML == "X" && i[8].innerHTML==i[5].innerHTML ||i[0].innerHTML==i[4].innerHTML && i[8].innerHTML == "X" && i[8].innerHTML==i[0].innerHTML || i[2].innerHTML==i[4].innerHTML && i[6].innerHTML == "X" && i[6].innerHTML==i[4].innerHTML    )
    {
        document.getElementById("indicadorVencedor").innerHTML="O vencedor foi: X "//retornar que o jogador X ganhou
        continuar = 0
    }else 
    {
        if( i[0].innerHTML==i[1].innerHTML && i[2].innerHTML == "O" && i[2].innerHTML==i[1].innerHTML || i[3].innerHTML==i[4].innerHTML && i[5].innerHTML == "O" && i[4].innerHTML==i[5].innerHTML || i[6].innerHTML==i[7].innerHTML && i[8].innerHTML == "O" && i[7].innerHTML==i[8].innerHTML || i[0].innerHTML==i[3].innerHTML && i[6].innerHTML == "O" && i[6].innerHTML==i[3].innerHTML || i[1].innerHTML==i[4].innerHTML && i[7].innerHTML == "O" && i[7].innerHTML==i[1].innerHTML || i[2].innerHTML==i[5].innerHTML && i[8].innerHTML == "O" && i[8].innerHTML==i[5].innerHTML ||i[0].innerHTML==i[4].innerHTML && i[8].innerHTML == "O" && i[8].innerHTML==i[0].innerHTML || i[2].innerHTML==i[4].innerHTML && i[6].innerHTML == "O" && i[6].innerHTML==i[4].innerHTML    )
        {
            document.getElementById("indicadorVencedor").innerHTML="O vencedor foi: O " //o vencedor é o jogador O
            continuar = 0
        }
        else 
        {
            if (contador>8){
                document.getElementById("indicadorVencedor").innerHTML="Deu velha" // deu velha.
                continuar = 0
            }
        }
    }
}
