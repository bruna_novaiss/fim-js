//Nessa variável estão todas as perguntas e respostas que vocês usarão no desafio
var cont=0
var perguntas = [
    {
        texto: 'O céu não é o limite',
        respostas: [
            { valor: 0, texto: 'Ter preguiça' },
            { valor: 1, texto: 'Quando der certo a gente faz' },
            { valor: 3, texto: 'Vamo pra cima familia' },
            { valor: 2, texto: 'Se der for viavel a gente faz' }
        ]
    },
    {
        texto: 'Apaixonados pela missão',
        respostas: [
            { valor: 0, texto: 'Projetos nada mais sao do que numeros' },
            { valor: 1, texto: 'Dinheiro é bom e é isto' },
            { valor: 3, texto: 'Se meu projeto nao ajudar meu cliente entao nao fiz nada' },
            { valor: 2, texto: 'Se eu entender o que esta no codigo entao ta show' }
        ]
    },
    {
        texto: 'Ohana',
        respostas: [
            { valor: 0, texto: 'Ta com problema? Se vira filhao' },
            { valor: 3, texto: 'Voce é meu irmao de batalha, to contigo e nao solto' },
            { valor: 2, texto: 'Voce é da gti, eu tambem entao tudo certo' },
            { valor: 1, texto: 'Fora da Gti nem olho' }
        ]
    },
    {
        texto: 'Integridade',
        respostas: [
            { valor: 0, texto: 'Furar a fila do ru' },
            { valor: 2, texto: 'farol amarelo pra mim é verde' },
            { valor: 1, texto: 'Clonar cartao de credito' },
            { valor: 3, texto: 'Sou integro' }
        ]
    },
    {
        texto: 'Ser capitão da nave',
        respostas: [
            { valor: 3, texto: 'Eu serei o lider que eu quero pra mim' },
            { valor: 0, texto: 'Eu nao sabia, entao nao fiz' },
            { valor: 1, texto: 'Ve la, depois me conta' },
            { valor: 2, texto: 'To so esperando meu estagio aprovar pra sair da GTi' }
        ]
    },
    {
        texto: 'Quem é seu trainee favorito?',
        respostas: [
            { valor: 3, texto: 'Por que que que é a Bruna em?' },
            { valor: 0, texto: 'Qual quer outro que não seja a Bruna' },
            { valor: 1, texto: 'Gabriel Lima' },
            { valor: 2, texto: 'Tanto faz' }
        ]
    }
]
var i=-1
function mostrarQuestao() {
    if ( document.getElementsByClassName("check")[0].checked ||document.getElementsByClassName("check")[1].checked ||document.getElementsByClassName("check")[2].checked ||document.getElementsByClassName("check")[3].checked  || i==-1 ){
        document.getElementById("confirmar").innerHTML = "Próxima pergunta"//mudar o nome do botão,para prosseguir
        if(i>4){
            finalizarQuiz()
        }else{
            i++// função para percorrer as perguntas
            document.getElementById("titulo").innerHTML = perguntas[i].texto
            document.getElementById("listaRespostas").style.display = "block"// para aparecer as opções a serem marcadas
            if(i==5){
            document.getElementById("confirmar").innerHTML = "finalizar quiz"//mudar o nome do botão,para finalizar

            }
            for(var j=0 ;j<4;j++)// função para percorrer as respostas
            {
                if( document.getElementsByClassName("check")[j].checked && i >0 ){
                    cont = cont + perguntas[i-1].respostas[j].valor // acessar o vetor perguntas e depois o respostas para verificar seu valor
                }
                document.getElementsByClassName("check")[j].checked = false // desmarcar para o item seguinte
                document.getElementsByClassName("alt")[j].innerHTML = perguntas[i].respostas[j].texto//acessar a primeira resposta da pergunta zero
            }
        }
    }

}

function finalizarQuiz() {
    document.getElementById("confirmar").style.display = "none"
    document.getElementById("titulo").innerHTML = "O resultado é: " + (cont/ 15 ) * 100 + "%"// mostrar a porcetagem de acertos da pessoa
    document.getElementById("listaRespostas").style.display = "none"// fazer as alternativas não aparecerem mais
}